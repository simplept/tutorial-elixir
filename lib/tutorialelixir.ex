defmodule Tutorialelixir do
  @moduledoc """
  Documentation for Tutorialelixir.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Tutorialelixir.hello
      :world

  """
  def hello do
    :world
  end
end
